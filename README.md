# Fórum de Discussão do EduForumUniverse

Bem-vindo ao Fórum de Discussão do EduForumUniverse! Esta aplicação é um espaço interativo onde os usuários da plataforma de LMS SaaS da EduForumUniverse podem se conectar, compartilhar conhecimento, tirar dúvidas e discutir sobre os cursos e conteúdos disponíveis.

## Visão Geral

O Fórum de Discussão do EduForumUniverse é uma parte essencial da nossa plataforma, permitindo que você:

- **Interaja com a Comunidade:** Participe de discussões, faça perguntas e compartilhe suas perspectivas sobre uma variedade de tópicos relacionados aos cursos e conteúdos.

- **Compartilhe Conhecimento:** Colabore com outros membros, compartilhe experiências e conhecimentos, e ajude seus colegas na jornada de aprendizado.

- **Tire Dúvidas:** Encontre respostas para suas dúvidas, tanto técnicas quanto de conteúdo, com a ajuda de especialistas e colegas de estudo.

- **Expanda seu Conhecimento:** Explore tópicos relacionados aos cursos que você está estudando, descubra recursos adicionais e aprofunde sua compreensão.

## Configurações Inicial
#### 1. Clone o repositório do projeto:
<https://gitlab.com/joaovitormonsoresnascimento/eduforumuniverse>

#### 2. Configure as variáveis de ambiente do backend:

modifique o arquivo env.example pasta backend/env.example para ".env"
(o arquivo deve ficar assim):

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=projectdatabase
DB_USERNAME=root
DB_PASSWORD=password
APP_DEBUG=true
APP_KEY=base64:UkOEwPoC5N9FZ7P5dprzxL3lL/PfkuJ4cZZLpUnIXSI=
#### 3. Instale as dependências:

rode os comandos:

`cd frontend && npm i && cd ..` - instala as dependências do frontend

`cd backend && composer i && cd ..` - instala as dependências do backend

#### 4. Inicie o projeto usando docker-compose:

na pasta do projeto, abra um terminal e utilize o comando `docker-compose up -d`

#### 5. Execute as migrações e semeie o banco de dados:

utilize o comando `php artisan migrate && php artisan db:seed`

caso ele peça permissão, conceda provavelmente digitando "yes" no terminal.

#### 6. Acesse o projeto!

em caso de falha verificar log do container do frontend
ou
```bash
cd frontend 
npm start
```
se você seguiu todas as etapas corretamente, poderá acessar o projeto através da rota http://localhost:3000/

## Como Começar
1. **Registro e Login:** Para começar a usar o fórum, é necessário criar uma conta ou fazer login com suas credenciais.

3. **Participe das Conversas:** Contribua com suas ideias, faça perguntas e compartilhe seu conhecimento participando de tópicos de discussão.



4. **Ajude a Comunidade:** Seja parte ativa da comunidade, respondendo a perguntas e fornecendo insights valiosos.

5. **Aprenda e Cresça:** Aproveite a experiência para aprimorar seu aprendizado e ajudar outros estudantes.


2. **Explorar Tópicos:** Após fazer login, você pode explorar os tópicos existentes no fórum. Use a barra de pesquisa para encontrar tópicos específicos.

3. **Participar das Discussões:** Você pode adicionar comentários a tópicos existentes ou criar seus próprios tópicos de discussão.

4. **Seguir Membros:** Siga outros membros para acompanhar suas atividades e receber notificações sobre suas postagens.

5. **Compartilhar Recursos:** Se você encontrar um recurso útil, compartilhe-o com a comunidade.

## Sobre

Este projeto é de código aberto, e foi desenvolvido para realização de um desafio da vaga full-stack da g1-learn
## Capturas de tela
![Alt text](image-4.png)
![Alt text](image-6.png)
![Alt text](image-5.png)

![Alt text](image.png)
![Alt text](image-1.png)
![Alt text](image-2.png)
![Alt text](image-3.png)


## Contato

Se você tiver alguma dúvida ou precisar de assistência, entre em contato conosco envie um e-mail para [joaovitormonsores@hotmail.com](mailto:joaovitormonsores@hotmail.com).
