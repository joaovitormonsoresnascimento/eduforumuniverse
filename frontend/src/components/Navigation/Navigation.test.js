import React from 'react';
import { render, screen, fireEvent, act } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Navigation from './Navigation';

describe('Navigation', () => {
  it('deve renderizar o componente Navigation corretamente', () => {
    render(
      <MemoryRouter>
        <Navigation />
      </MemoryRouter>
    );

    // Verifica se elementos de interface estão presentes
    expect(screen.getByAltText('logo')).toBeInTheDocument();
    expect(screen.getByText('Home')).toBeInTheDocument();
    expect(screen.getByText('Usuários')).toBeInTheDocument();
  });

  it('deve lidar com o clique nas opções de navegação', () => {
    const navigate = jest.fn();

    render(
      <MemoryRouter>
        <Navigation />
      </MemoryRouter>
    );

    const homeLink = screen.getByText('Home');
    const usersLink = screen.getByText('Usuários');

    fireEvent.click(homeLink);
    expect(navigate).toHaveBeenCalledWith('/home');

    fireEvent.click(usersLink);
    expect(navigate).toHaveBeenCalledWith('/users');
  });

  it('deve lidar com o clique no botão de logout', async () => {
    global.fetch = jest.fn().mockResolvedValue({ ok: true });

    render(
      <MemoryRouter>
        <Navigation />
      </MemoryRouter>
    );

    const logoutButton = screen.getByText('Sair');
    fireEvent.click(logoutButton);

    await act(async () => {
      // Aguarda a resolução da função fetch
    });

    expect(global.fetch).toHaveBeenCalledWith('http://localhost:8000/api/logout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer null', // Verifica se o token é removido
      },
    });

    expect(localStorage.getItem('token')).toBeNull();
    expect(localStorage.getItem('user_id')).toBeNull();
  });
});
