import React from 'react';
import { render, screen } from '@testing-library/react';
import Form from './Form';

test('Renderiza o componente Form', () => {
  render(<Form />);
  const formElement = screen.getByTestId('form');
  expect(formElement).toBeInTheDocument();
});
