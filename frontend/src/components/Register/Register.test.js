import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import Register from './Register';

// Mock da função fetch para simular a chamada à API
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () =>
      Promise.resolve({
        status: 'success',
        message: 'Cadastro realizado com sucesso.',
      }),
    ok: true,
  })
);

describe('Register', () => {
  it('deve lidar com o registro com sucesso', async () => {
    render(<Register />);

    // Simula a interação do usuário preenchendo os campos de registro
    fireEvent.change(screen.getByPlaceholderText('Nome *'), {
      target: { value: 'João' },
    });
    fireEvent.change(screen.getByPlaceholderText('Sobrenome *'), {
      target: { value: 'Vitor' },
    });
    fireEvent.change(screen.getByPlaceholderText('E-mail *'), {
      target: { value: 'joao@example.com' },
    });
    fireEvent.change(screen.getByPlaceholderText('Senha *'), {
      target: { value: 'senha123' },
    });
    fireEvent.change(screen.getByPlaceholderText('Confirme a senha *'), {
      target: { value: 'senha123' },
    });

    fireEvent.click(screen.getByText('Registrar'));

    // Aguarda a resposta da API
    await waitFor(() => {
      expect(screen.getByText('Cadastro realizado com sucesso.')).toBeInTheDocument();
    });
  });
});
