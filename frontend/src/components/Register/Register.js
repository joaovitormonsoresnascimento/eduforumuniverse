import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Register.css';

export default function Register() {
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const [alertType, setAlertType] = useState(''); 
  const [lastname, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  const handleRegister = async (e) => {
    e.preventDefault();

    try {
      const fullName = name + ' ' + lastname; 
      const response = await fetch('http://localhost:8000/api/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-TOKEN': 'y1G9JLSNOAn5TkeaxbvCqN6zLlrYZRVE2i74tR1Q'
        },
        body: JSON.stringify({
          name: fullName,
          email,
          password,
          password_confirmation: passwordConfirmation,
        }),
      });

      if (response.ok) {
        setAlertMessage('Cadastro realizado com sucesso.');
        setAlertType('success');
        setTimeout(() => {
          setAlertMessage('');
          setAlertType('');
          navigate('/');
        }, 3000); 

      } else {
        setAlertMessage('Erro ao cadastrar usuário. Verifique os dados e tente novamente.');
        setAlertType('danger');
        setTimeout(() => {
          setAlertMessage('');
          setAlertType('');
        }, 3000);

      }
    } catch (error) {
      console.log(error); // Registra o erro no console

      setAlertMessage('Ocorreu um erro na requisição. Por favor, tente novamente.');
      setAlertType('danger'); // Define o tipo de mensagem como erro
    }
  };
  const getAlertClass = () => {
    if (alertType === 'success') {
      return 'alert alert-success';
    } else if (alertType === 'danger') {
      return 'alert alert-danger';
    }
    return '';
  };

  return (
    <div>
      <div className='register-page'>
        <img
          className="header-logo"
          src="https://g1learn.com/logo-.png"
          alt="Logo da minha aplicação"
        />

        <div  className={`register-form-box ${alertMessage ? 'expanded' : ''}`}>
          {alertMessage && ( 
            <div className={getAlertClass()} role="alert">
              {alertMessage}
            </div>
          )}
          <h3 className="fw-medium">Cadastre-se</h3>
          <p className="lead">Digite seus dados.</p>
          <form className="mt-4 register-form" onSubmit={handleRegister}>
            <div className="row">
              <div className="col-sm-6">
                <div className="mb-3">
                  <input
                    required
                    type="text"
                    id="name"
                    className="form-control"
                    placeholder="Nome *"
                    aria-label="name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                  <div className="invalid-feedback" style={{ display: 'block' }}></div>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="mb-3">
                  <input
                    required
                    type="text"
                    id="lastname"
                    className="form-control"
                    placeholder="Sobrenome *"
                    aria-label="lastname"
                    value={lastname}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                  <div className="invalid-feedback" style={{ display: 'block' }}></div>
                </div>
              </div>
            </div>
            <div className="mb-3">
              <input
                required
                type="email"
                id="email"
                className="form-control"
                placeholder="E-mail *"
                aria-label="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <div className="invalid-feedback" style={{ display: 'block' }}></div>
            </div>
            <div className="row">
              <div className="col-sm-6">
                <div className="mb-3">
                  <input
                    required
                    type="password"
                    id="password"
                    className="form-control"
                    placeholder="Senha *"
                    aria-label="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <div className="invalid-feedback" style={{ display: 'block' }}></div>
                </div>
              </div>
              <div className="col-sm-6">
                <div className="mb-3">
                  <input
                    required
                    type="password"
                    id="passwordConfirmation"
                    className="form-control"
                    placeholder="Confirme a senha *"
                    aria-label="passwordConfirmation"
                    value={passwordConfirmation}
                    onChange={(e) => setPasswordConfirmation(e.target.value)}
                  />
                  <div className="invalid-feedback" style={{ display: 'block' }}></div>
                </div>
              </div>
            </div>
            <button className="btn btn-primary register-button">Registrar</button>
          </form>
          <div className="d-flex justify-content-center mt-3"> 
            <p className='login-now'>
              Já registrado? <span onClick={() => navigate('/')}>Faça login!</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
