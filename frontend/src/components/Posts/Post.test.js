import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import PostDetails from './PostDetails';

// Mock de funções e objetos necessários
jest.mock('react-router-dom', () => ({
  useNavigate: () => jest.fn(),
  useParams: () => ({ id: '1' }),
}));

global.localStorage = {
  getItem: jest.fn(),
};

global.fetch = jest.fn();

describe('PostDetails', () => {
  beforeEach(() => {
    fetch.mockClear();
    localStorage.getItem.mockClear();
  });

  it('deve renderizar o componente PostDetails', async () => {
    // Mock da resposta da API para os dados do post e dos comentários
    const mockData = {
      id: '1',
      title: 'Título do Post',
      user: 'Usuário',
      created_at: '2023-10-25T10:00:00Z',
      content: 'Conteúdo do post',
      category: 'Categoria do Post',
    };

    const mockComments = [
      {
        id: '1',
        user: 'Usuário Comentário',
        created_at: '2023-10-25T12:00:00Z',
        content: 'Conteúdo do comentário',
      },
    ];

    fetch.mockImplementation((url) => {
      if (url.endsWith('/api/posts/1')) {
        return Promise.resolve({
          ok: true,
          json: () => Promise.resolve(mockData),
        });
      } else if (url.endsWith('/api/posts/1/comments')) {
        return Promise.resolve({
          ok: true,
          json: () => Promise.resolve(mockComments),
        });
      }
    });

    render(<PostDetails />);
    
    // Aguarda que os elementos sejam renderizados
    await waitFor(() => {
      expect(screen.getByText('Título do Post')).toBeInTheDocument();
      expect(screen.getByText('Usuário')).toBeInTheDocument();
      expect(screen.getByText('Categoria do Post')).toBeInTheDocument();
      expect(screen.getByText('Conteúdo do post')).toBeInTheDocument();
      expect(screen.getByText('Usuário Comentário')).toBeInTheDocument();
      expect(screen.getByText('Conteúdo do comentário')).toBeInTheDocument();
      expect(screen.getByText('Novo comentário')).toBeInTheDocument();
    });
  });

  it('deve manipular o envio de um novo comentário', async () => {
    render(<PostDetails />);

    // Simula a interação do usuário preenchendo o campo de novo comentário
    fireEvent.change(screen.getByPlaceholderText('Digite seu comentário...'), {
      target: { value: 'Novo comentário de teste' },
    });

    // Simula o clique no botão "Enviar"
    fireEvent.click(screen.getByText('Enviar'));

    // Aguarda a resposta da API
    await waitFor(() => {
      expect(screen.getByText('Novo comentário de teste')).toBeInTheDocument();
    });
  });


});
