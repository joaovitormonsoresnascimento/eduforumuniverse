import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import PostForm from './PostForm';

// Mock de funções e objetos necessários
global.localStorage = {
  getItem: jest.fn(),
};

describe('PostForm', () => {
  it('deve renderizar o componente PostForm', () => {
    // Mock das categorias
    const mockCategories = [
      { id: '1', name: 'Categoria 1' },
      { id: '2', name: 'Categoria 2' },
    ];

    render(
      <PostForm
        newPost={{
          title: 'Título de Teste',
          content: 'Conteúdo de Teste',
          category_id: '1',
        }}
        categories={mockCategories}
        handleInputChange={() => {}}
        createPost={() => {}}
      />
    );

    // Verifica se os elementos do formulário são renderizados corretamente
    expect(screen.getByText('Título:')).toBeInTheDocument();
    expect(screen.getByText('Conteúdo:')).toBeInTheDocument();
    expect(screen.getByText('Categoria:')).toBeInTheDocument();
    expect(screen.getByText('Selecionar uma categoria')).toBeInTheDocument();
    expect(screen.getByText('Criar post')).toBeInTheDocument();
  });

  it('deve manipular o envio do formulário', () => {
    // Função mock para lidar com o envio do formulário
    const createPostMock = jest.fn();

    // Mock das categorias
    const mockCategories = [
      { id: '1', name: 'Categoria 1' },
      { id: '2', name: 'Categoria 2' },
    ];

    render(
      <PostForm
        newPost={{
          title: 'Título de Teste',
          content: 'Conteúdo de Teste',
          category_id: '1',
        }}
        categories={mockCategories}
        handleInputChange={() => {}}
        createPost={createPostMock}
      />
    );

    // Simula o preenchimento do formulário
    fireEvent.change(screen.getByLabelText('Título:'), {
      target: { value: 'Novo Título' },
    });
    fireEvent.change(screen.getByLabelText('Conteúdo:'), {
      target: { value: 'Novo Conteúdo' },
    });
    fireEvent.change(screen.getByLabelText('Categoria:'), {
      target: { value: '2' },
    });

    // Simula o envio do formulário
    fireEvent.click(screen.getByText('Criar post'));

    // Verifica se a função createPost foi chamada com os valores corretos
    expect(createPostMock).toHaveBeenCalledWith({
      title: 'Novo Título',
      content: 'Novo Conteúdo',
      category_id: '2',
    });
  });


});
