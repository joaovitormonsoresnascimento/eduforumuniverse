import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import PostList from './PostList';

describe('PostList', () => {
  it('deve renderizar o componente PostList com dados', () => {
    const mockPosts = {
      data: [
        {
          id: 1,
          user: 'User 1',
          title: 'Post 1',
          category: 'Category 1',
          created_at: '2023-10-25T10:00:00.000Z',
        },
        {
          id: 2,
          user: 'User 2',
          title: 'Post 2',
          category: 'Category 2',
          created_at: '2023-10-25T11:00:00.000Z',
        },
      ],
    };

    const navigateMock = jest.fn();
    const pageCount = 1;
    const handlePageClickMock = jest.fn();

    render(
      <PostList
        posts={mockPosts}
        navigate={navigateMock}
        pageCount={pageCount}
        handlePageClick={handlePageClickMock}
      />
    );

    // Verifica se os elementos da tabela são renderizados corretamente
    expect(screen.getByText('Autor')).toBeInTheDocument();
    expect(screen.getByText('Título')).toBeInTheDocument();
    expect(screen.getByText('Categoria')).toBeInTheDocument();
    expect(screen.getByText('Criação')).toBeInTheDocument();

    // Verifica se os dados dos posts são renderizados corretamente
    expect(screen.getByText('User 1')).toBeInTheDocument();
    expect(screen.getByText('Post 1')).toBeInTheDocument();
    expect(screen.getByText('Category 1')).toBeInTheDocument();
    expect(screen.getByText('25/10/2023 10:00:00')).toBeInTheDocument();
    expect(screen.getByText('User 2')).toBeInTheDocument();
    expect(screen.getByText('Post 2')).toBeInTheDocument();
    expect(screen.getByText('Category 2')).toBeInTheDocument();
    expect(screen.getByText('25/10/2023 11:00:00')).toBeInTheDocument();

    // Verifica se os botões de paginação são renderizados
    expect(screen.getByText('Anterior')).toBeInTheDocument();
    expect(screen.getByText('Próxima')).toBeInTheDocument();
  });

  it('deve chamar a função de navegação ao clicar em um post', () => {
    const mockPosts = {
      data: [
        {
          id: 1,
          user: 'User 1',
          title: 'Post 1',
          category: 'Category 1',
          created_at: '2023-10-25T10:00:00.000Z',
        },
      ],
    };

    const navigateMock = jest.fn();
    const pageCount = 1;
    const handlePageClickMock = jest.fn();

    render(
      <PostList
        posts={mockPosts}
        navigate={navigateMock}
        pageCount={pageCount}
        handlePageClick={handlePageClickMock}
      />
    );

    // Simula o clique no primeiro post
    fireEvent.click(screen.getByText('Post 1'));

    // Verifica se a função de navegação foi chamada com o ID correto do post
    expect(navigateMock).toHaveBeenCalledWith('/posts/1');
  });

});
