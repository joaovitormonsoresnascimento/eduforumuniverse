import React, { useState, useEffect } from 'react';
import './Post.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { format } from 'date-fns';

const PostList = ({ posts, navigate, pageCount, handlePageClick }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const recordsPerPage = 5;

  const nPage = posts.data ? Math.ceil(posts.data.length / recordsPerPage) : 0;
  const numbers = [...Array(nPage).keys()].map((num) => num + 1);
  const lastIndex = currentPage * recordsPerPage;
  const firstIndex = lastIndex - recordsPerPage;
  const records = posts.data && posts.data.length > 0 ? posts.data.slice(firstIndex, lastIndex) : [];

  const handlePostClick = (postId) => {
    navigate(`/posts/${postId}`);
  };

  const prePage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  
  const nextPage = () => {
    if (currentPage < nPage) {
      setCurrentPage(currentPage + 1);
    }
  };
  

  useEffect(() => {
    if (!Array.isArray(posts.data)) {
      console.log("Erro: posts não é uma matriz", posts);
    }
  }, [posts]);

  return (
    <div>
      <table className="table table-light table-striped table-hover">
        <thead className="thead-dark">
          <tr>
            <th>Autor</th>
            <th>Título</th>
            <th>Categoria</th>
            <th>Criação</th>
          </tr>
        </thead>
        <tbody>
          {records.map((post) => (
            <tr onClick={() => handlePostClick(post.id)} key={post.id}>
              <td>
                <div className="align-left">
                  <div className="author-image">
                    <img
                      src="/images/user.png"
                      alt="Imagem do Autor"
                      width={50}
                      height={50}
                    />
                  </div>
                  <p className="title-post">{post.user}</p>
                </div>
              </td>
              <td>{post.title}</td>
              <td>{post.category}</td>
              <td>{format(new Date(post.created_at), 'dd/MM/yyyy HH:mm:ss')}</td>
            </tr>
          ))}
        </tbody>
      </table>

      <ul className="pagination-numbers">
        <li className='page-item'>
          <a href='#' className='page-link' onClick={prePage}>Anterior</a>
        </li>
        {numbers.map((number) => (
          <li key={number} className={number === currentPage ? 'page-item active' : 'page-item'} onClick={() => setCurrentPage(number)}>
            {number}
          </li>
        ))}
        <li className='page-item'>
          <a href='#' className='page-link' onClick={nextPage}>Próxima</a>
        </li>
      </ul>
    </div>
  );
};

export default PostList;
