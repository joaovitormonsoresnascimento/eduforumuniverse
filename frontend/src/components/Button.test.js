// Button.test.js
import React from 'react';
import { render, screen } from '@testing-library/react';
import Button from './Button';

test('Renderiza o componente Button', () => {
  render(<Button label="Clique em mim" />);
  const buttonElement = screen.getByText('Clique em mim');
  expect(buttonElement).toBeInTheDocument();
});
