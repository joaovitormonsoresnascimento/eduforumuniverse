  import React, { useState } from 'react';
  import { useNavigate } from 'react-router-dom';
  import 'bootstrap/dist/css/bootstrap.min.css';
  import './Login.css';

  export default function Login() {
    const [alertMessage, setAlertMessage] = useState('');
    const navigate = useNavigate();
    const [credentials, setCredentials] = useState({ email: '', password: '' });
    const [alertType, setAlertType] = useState(''); // Estado para o tipo de mensagem
    const [isLoading, setIsLoading] = useState(false); // Variável para controlar o spinner

    const handleInputChange = (e) => {
      const { name, value } = e.target;
      setCredentials((prevCredentials) => ({
        ...prevCredentials,
        [name]: value,
      }));
    };

    const handleLogin = async (e) => {
      e.preventDefault();

      try {
        setIsLoading(true);

        const response = await fetch('http://localhost:8000/api/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': 'y1G9JLSNOAn5TkeaxbvCqN6zLlrYZRVE2i74tR1Q'
          },
          body: JSON.stringify(credentials),
        });


        if (response.ok) {
          const data = await response.json();
          const { token, user } = data;
          const userId = user.id;
          setAlertMessage('Usuario Logado Com Sucesso!');
          setAlertType('success'); // Define o tipo de mensagem como sucesso
          localStorage.setItem('user_id', userId);
          localStorage.setItem('token', token);
          setTimeout(() => {
            setAlertMessage('');
            setAlertType('');
            navigate('/home');
          }, 3000);

        } else {
          setAlertMessage('Usuario ou Senha Invalidos. Por favor, tente novamente.');
          setAlertType('danger'); // Define o tipo de mensagem como erro
          // Limpa a mensagem após 3 segundos
          setTimeout(() => {
            setAlertMessage('');
            setAlertType('');
            setIsLoading(false);
          }, 3000);
        }
      } catch (error) {
        setAlertMessage('Ocorreu um erro na requisição. Por favor, tente novamente.');
        setAlertType('danger'); // Define o tipo de mensagem como erro
        setIsLoading(true);
      }
    };
    const getAlertClass = () => {
      if (alertType === 'success') {
        return 'alert alert-success';
      } else if (alertType === 'danger') {
        return 'alert alert-danger';
      }
      return ''; // Se nenhum tipo estiver definido, não aplique nenhuma classe
    };
    return (
      <div className='page-login'>
        <img
          onClick={() => navigate('/')}
          className="header-logo"
          src="https://g1learn.com/logo-.png"
          alt="Logo da minha aplicação"
        />
        <div className='container'>
          <div className='login-form-box'>
            { alertMessage && ( // Exibir o alerta apenas se houver uma mensagem
              <div className={getAlertClass()} role="alert">
                {alertMessage}
              </div>

            )}
            {isLoading &&( 
              <div id="spinner" className="spinner" ></div>
            )}
            <form className='login-form mb-3' onSubmit={handleLogin}>
              <div className='mb-3'>
                <label htmlFor="email" className='form-label'>Email:</label>
                <input
                  type="text"
                  id="email"
                  name="email"
                  value={credentials.email}
                  onChange={handleInputChange}
                  className='form-control'
                />
              </div>
              <div className='mb-3'>
                <label htmlFor="password" className='form-label'>Senha:</label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  value={credentials.password}
                  onChange={handleInputChange}
                  className='form-control'
                />
              </div>
              <button className='login-button' type="submit">Login</button>
            </form>
            <p className='register-now '>
              Novo usuário? <span onClick={() => navigate('/register')}>Registre-se</span> agora!
            </p>
          </div>
        </div>
      </div>
    );
  }
