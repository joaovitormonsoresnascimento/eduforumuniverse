import React from 'react';
import { render, screen, fireEvent, waitFor, act } from '@testing-library/react';
import { MemoryRouter, Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import Login from './Login';

describe('Login', () => {
  it('deve renderizar o componente Login corretamente', () => {
    render(
      <MemoryRouter>
        <Login />
      </MemoryRouter>
    );

    // Verifica se elementos de interface estão presentes
    expect(screen.getByText('Email:')).toBeInTheDocument();
    expect(screen.getByText('Senha:')).toBeInTheDocument();
    expect(screen.getByText('Login')).toBeInTheDocument();
    expect(screen.getByText('Novo usuário?')).toBeInTheDocument();
  });

  it('deve lidar com a submissão do formulário de login', async () => {
    const history = createMemoryHistory();
    const mockUser = {
      id: 1,
      email: 'example@example.com',
      password: 'password',
    };
    const mockToken = 'mockToken';

    const response = {
      ok: true,
      json: jest.fn().mockResolvedValue({ user: mockUser, token: mockToken }),
    };
    global.fetch = jest.fn().mockResolvedValue(response);

    render(
      <Router history={history}>
        <Login />
      </Router>
    );

    const emailInput = screen.getByLabelText('Email:');
    const passwordInput = screen.getByLabelText('Senha:');
    const loginButton = screen.getByText('Login');

    fireEvent.change(emailInput, { target: { value: mockUser.email } });
    fireEvent.change(passwordInput, { target: { value: mockUser.password } });

    fireEvent.click(loginButton);

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledTimes(1);
      expect(fetch).toHaveBeenCalledWith('http://localhost:8000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-TOKEN': 'y1G9JLSNOAn5TkeaxbvCqN6zLlrYZRVE2i74tR1Q',
        },
        body: JSON.stringify({ email: mockUser.email, password: mockUser.password }),
      });

      expect(history.location.pathname).toBe('/home');

      // Verifica se a mensagem de sucesso foi exibida
      expect(screen.getByText('Usuario Logado Com Sucesso!')).toBeInTheDocument();
    });
  });

  it('deve lidar com a falha na submissão do formulário de login', async () => {
    const history = createMemoryHistory();
    const mockUser = {
      email: 'example@example.com',
      password: 'password',
    };

    const response = {
      ok: false,
    };
    global.fetch = jest.fn().mockResolvedValue(response);

    render(
      <Router history={history}>
        <Login />
      </Router>
    );

    const emailInput = screen.getByLabelText('Email:');
    const passwordInput = screen.getByLabelText('Senha:');
    const loginButton = screen.getByText('Login');

    fireEvent.change(emailInput, { target: { value: mockUser.email } });
    fireEvent.change(passwordInput, { target: { value: mockUser.password } });

    fireEvent.click(loginButton);

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledTimes(1);
      expect(fetch).toHaveBeenCalledWith('http://localhost:8000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-TOKEN': 'y1G9JLSNOAn5TkeaxbvCqN6zLlrYZRVE2i74tR1Q',
        },
        body: JSON.stringify({ email: mockUser.email, password: mockUser.password }),
      });

      // Verifica se a mensagem de erro foi exibida
      expect(screen.getByText('Usuario ou Senha Invalidos. Por favor, tente novamente.')).toBeInTheDocument();
    });
  });
});
