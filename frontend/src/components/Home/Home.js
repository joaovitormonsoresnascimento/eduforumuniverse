import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Navigation from '../../components/Navigation/Navigation';
import PostForm from '../../components/Posts/PostForm';
import PostList from '../../components/Posts/PostList';
import './Home.css';

export default function Home() {
  const [posts, setPosts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [newPost, setNewPost] = useState({ title: '', content: '', category_id: '' });
  const [selectedCategory, setSelectedCategory] = useState('');
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [alertType, setAlertType] = useState('');
  const [alertMessage, setAlertMessage] = useState('');


  
  const fetchPosts = useCallback(async () => {
    try {
      let url = 'http://localhost:8000/api/posts';
      if (selectedCategory) {
        url += `?category_id=${selectedCategory}`;
      }
  
      // Mostrar o spinner
      document.getElementById('spinner').style.display = 'block';
  
      const response = await fetch(url, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
  
      if (!response.ok) {
        throw new Error(`Erro na requisição: ${response.status} - ${response.statusText}`);
      }
  
      const data = await response.json();
      setPosts(data);
    } catch (error) {
      console.error('Erro na solicitação de posts:', error);
      handleAlert('Erro ao buscar os posts', 'error');
    } finally {
      // Esconder o spinner, independentemente de ter ocorrido um erro ou não
      document.getElementById('spinner').style.display = 'none';
    }
  }, [selectedCategory]);
  
  const fetchCategories = useCallback(async () => {
    try {
      // Mostrar o spinner
      document.getElementById('spinner').style.display = 'block';
  
      const response = await fetch('http://localhost:8000/api/categories', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      });
  
      if (!response.ok) {
        throw new Error(`Erro na requisição: ${response.status} - ${response.statusText}`);
      }
  
      const data = await response.json();
      setCategories(data);
      setLoading(false);
    } catch (error) {
      console.error('Erro na solicitação de categorias:', error);
      setCategories('');
      handleAlert('Erro ao buscar as categorias', 'error');
    } finally {
      // Esconder o spinner, independentemente de ter ocorrido um erro ou não
      document.getElementById('spinner').style.display = 'none';
    }
  }, []);
  
  
  const handleAlert = (message, type) => {
    setAlertMessage(message);
    setAlertType(type);
    setTimeout(() => {
      setAlertMessage('');
      setAlertType('');
    }, 3000);
  };

  const createPost = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch('http://localhost:8000/api/posts', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(newPost),
      });

      const responseData = await response.json();

      if (responseData.retorno === 'Post criado!') {
        setNewPost({ title: '', content: '', category_id: '' });
        handleAlert('Post Salvo com Sucesso', 'success');
        fetchPosts();
      } else {
        handleAlert('Erro ao criar o post. Verifique os campos e tente novamente.', 'danger');
      }
    } catch (error) {
      handleAlert('Falha ao Criar Post - Erro na requisição', 'danger');
    }
  };

  const handlePostFormInputChange = (e) => {
    setNewPost((prevPost) => ({ ...prevPost, [e.target.name]: e.target.value }));
  };

  const handleCategoryChange = (e) => {
    setSelectedCategory(e.target.value);
  };

  const getAlertClass = () => {
    if (alertType === 'success') {
      return 'alert alert-success';
    } else if (alertType === 'danger') {
      return 'alert alert-danger';
    }
    return '';
  };

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      alert('Erro de autorização');
      navigate('/');
    }

    fetchCategories();
    fetchPosts();
  }, [navigate, fetchCategories, fetchPosts]);

  return (
    <>
      <Navigation />
      <div className='home-page'>
      <div id="spinner" className="spinner"></div>
        {!loading && categories.length > 0 && (
          <div className="category-selector">
            <label>Categoria: </label>
            <select value={selectedCategory} onChange={handleCategoryChange}>
              <option value="">Todas as categorias</option>
              {categories.map((category) => (
                <option key={category.id} value={category.id}>
                  {category.name}
                </option>
              ))}
            </select>
          </div>
        )}

        {!loading && categories.length > 0 && (
          <PostList posts={posts} navigate={navigate} />
        )}

        <div className="page-divider">
          <h2 className="addnewpost-title">Adicione uma nova discussão</h2>
        </div>

        {alertMessage && (
          <div className={getAlertClass()} role="alert">
            {alertMessage}
          </div>
        )}

        <PostForm
          newPost={newPost}
          categories={categories}
          handleInputChange={handlePostFormInputChange}
          createPost={createPost}
        />
      </div>
    </>
  );
}
