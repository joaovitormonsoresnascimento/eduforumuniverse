import React from 'react';
import { render, screen, fireEvent, waitFor, act } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Home from './Home';

describe('Home', () => {
  it('deve renderizar o componente Home corretamente', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );

    // Verifica se elementos de interface estão presentes
    expect(screen.getByText('Categoria:')).toBeInTheDocument();
    expect(screen.getByText('Todas as categorias')).toBeInTheDocument();
    expect(screen.getByText('Adicione uma nova discussão')).toBeInTheDocument();
  });

  it('deve lidar com a seleção de categoria', () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );

    const categorySelector = screen.getByText('Categoria:').nextElementSibling;
    fireEvent.change(categorySelector, { target: { value: '1' } });

    expect(categorySelector.value).toBe('1');
  });

  it('deve lidar com a criação de um novo post', async () => {
    render(
      <MemoryRouter>
        <Home />
      </MemoryRouter>
    );

    const newPostTitleInput = screen.getByLabelText('Título:');
    const newPostContentInput = screen.getByLabelText('Conteúdo:');
    const createPostButton = screen.getByText('Criar post');

    fireEvent.change(newPostTitleInput, { target: { value: 'Novo Título' } });
    fireEvent.change(newPostContentInput, { target: { value: 'Conteúdo do novo post' } });

    const mockResponse = { retorno: 'Post criado!' };
    global.fetch = jest.fn().mockResolvedValue({
      ok: true,
      json: jest.fn().mockResolvedValue(mockResponse),
    });

    fireEvent.click(createPostButton);

    await waitFor(() => {
      expect(fetch).toHaveBeenCalledTimes(1);
      expect(fetch).toHaveBeenCalledWith('http://localhost:8000/api/posts', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({ title: 'Novo Título', content: 'Conteúdo do novo post', category_id: '' }),
      });

      expect(screen.getByText('Post Salvo com Sucesso')).toBeInTheDocument();
    });
  });
});
