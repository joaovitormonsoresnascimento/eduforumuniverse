import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './components/Login/Login';

import './index.css'
import Register from './components/Register/Register';
import Home from './components/Home/Home';
import PostDetails from './components/Posts/Post';

const App = () => {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/posts/:id" element={<PostDetails />} />
          <Route path="*" element={NotFound} />
        </Routes>
      </Router>
    </div>
  );
};

const NotFound = () => {
  return <h1>404 - Page Not Found</h1>;
};

export default App;
