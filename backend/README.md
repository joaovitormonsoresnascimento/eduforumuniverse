# Guia de Instalação e Execução da API Laravel

Este guia mostrará como instalar as dependências do Composer e executar sua API Laravel localmente. Certifique-se de ter o Composer instalado antes de prosseguir.

## Requisitos Prévios
- PHP >8.1 : Certifique-se de que o PHP 8.1 esteja instalado no seu sistema. Se não estiver instalado, você pode baixá-lo em [php.net](https://www.php.net/downloads.php).


- Composer: Certifique-se de ter o Composer instalado no seu sistema. Se não estiver instalado, você pode baixá-lo em [getcomposer.org](https://getcomposer.org/download/).

## Instalação

1. Instale as dependecias do seu projeto Laravel:

   ```bash
    composer i 
1. Realizar a migração e popular dados inicias:
   ```bash
    php artisan migrate && php artisan db:seed