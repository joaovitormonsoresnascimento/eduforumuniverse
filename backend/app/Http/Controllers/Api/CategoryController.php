<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function list()
    {
        $category = new Category();

        $categories = $category->all();

        return $categories;
    }


    public function select($categoryId)
    {
        $category = new Category();
        $categorySelected = $category->find($categoryId, 'name');

        if (!$categorySelected) {
            return response()->json(['retorno' => 'erro', 'mensagem' => 'Categoria não existe']);
        }

        return $categorySelected;
    }


    public function add(Request $request)
    {
        try {
            $category = new Category;
            $category->name = $request->input('name');
            $category->save();

            return response()->json(['retorno' => 'Categoria criada!', 'categoria' => $category]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function update(Request $request, $categoryId)
    {
        try {
            $category = new Category();
            $categorySelected = $category->find($categoryId);

            if (!$categorySelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Categoria não existe']);
            }

            $categorySelected->name = $request->input('name');
            $categorySelected->save();

            return response()->json(['retorno' => 'Categoria atualizada!', 'updated_data' => $request->only('name')]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function delete($categoryId)
    {
        try {
            $category = new Category();
            $categorySelected = $category->find($categoryId);

            if (!$categorySelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Categoria não existe']);
            }

            $categorySelected->delete();

            return response()->json(['retorno' => 'Categoria deletada']);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }
}
