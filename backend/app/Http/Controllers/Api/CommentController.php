<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    protected $auth;
    protected $user;
    public function __construct(User $user, Auth $auth)
    {
        $this->user = $user;
        $this->auth = $auth;
    }
    public function list()
    {
        $comments = Comment::with(['post', 'user'])->get();

        $formattedComments = $comments->map(function ($comment) {
            return [
                'id' => $comment->id,
                'user' => $comment->user->name,
                'user_id' => $comment->user->id,
                'post_id' => $comment->post->id,
                'content' => $comment->content,
                'created_at' => $comment->created_at,
                'updated_at' => $comment->updated_at
            ];
        });

        return $formattedComments;
    }

    public function select($commentId)
    {
        $comment = new Comment();
        $commentSelected = $comment->with(['post', 'user'])->find($commentId);

        if (!$commentSelected) {
            return response()->json(['retorno' => 'erro', 'mensagem' => 'Comentário não existe']);
        }

        $formattedComment = [
            'id' => $commentSelected->id,
            'user' => $commentSelected->user->name,
            'user_id' => $commentSelected->user->id,
            'post_id' => $commentSelected->post->id,
            'content' => $commentSelected->content,
            'created_at' => $commentSelected->created_at,
            'updated_at' => $commentSelected->updated_at
        ];

        return $formattedComment;
    }

    public function add(Request $request)
    {
        try {
            $user = auth()->user();
            $comment = $user->comments()->create([
                'content' => $request->content,
                'post_id' => $request->post_id
            ]);

            return response()->json([
                'retorno' => 'Comentário criado!',
                'comentário' => [
                    'content' => $comment->content,
                ]
            ]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function addCommentToPost(Request $request, $postId)
    {
        try {
            $user = auth()->user();
            $comment = $user->comments()->create([
                'content' => $request->content,
                'post_id' => $postId
            ]);

            return response()->json([
                'retorno' => 'Comentário criado!',
                'comentário' => [
                    'content' => $comment->content,
                ]
            ]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function listByPost($postId)
    {
        $comments = Comment::with(['post', 'user'])
            ->where('post_id', $postId)
            ->get();

        $formattedComments = $comments->map(function ($comment) {
            return [
                'id' => $comment->id,
                'user' => $comment->user->name,
                'user_id' => $comment->user->id,
                'post_id' => $comment->post->id,
                'content' => $comment->content,
                'created_at' => $comment->created_at,
                'updated_at' => $comment->updated_at
            ];
        });

        return $formattedComments;
    }

    public function updateComment(Request $request, $commentId)
    {
        try {
            $userAutenticado = auth()->user();
            $comment = new Comment();
            $commentSelected = $comment->find($commentId);

            if (!$commentSelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Comentário não existe']);
            }

            if ($commentSelected->user_id !== $userAutenticado->id) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Você não tem permissão para editar este comentário']);
            }

            $commentSelected->content = $request->content;
            $commentSelected->save();

            return response()->json([
                'retorno' => 'Comentário atualizado!',
                'comentário' => [
                    'id' => $commentSelected->id,
                    'user' => $commentSelected->user->name,
                    'post_id' => $commentSelected->post->id,
                    'content' => $commentSelected->content,
                    'created_at' => $commentSelected->created_at,
                    'updated_at' => $commentSelected->updated_at
                ]
            ]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function deleteComment($commentId)
    {
        try {
            $user = auth()->user();
            $comment = new Comment();
            $commentSelected = $comment->find($commentId);

            if (!$commentSelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Comentário não existe']);
            }

            if ($commentSelected->user_id !== $user->id) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Você não tem permissão para excluir este comentário']);
            }

            $commentSelected->delete();

            return response()->json(['retorno' => 'Comentário excluído com sucesso']);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }
}
