<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function list(Request $request)
    {
        $categoryId = $request->input('category_id');
        $orderBy = $request->input('orderBy', 'created_at');
        $perPage = $request->input('perPage', 10);
        $page = $request->input('page', 1);
        $post = new Post();
        $postsQuery = $post->with(['category', 'user']);

        if ($categoryId) {
            $postsQuery->where('category_id', $categoryId);
        }

        $totalPosts = $postsQuery->count();

        $posts = $postsQuery
            ->orderBy($orderBy, 'desc')
            ->skip(($page - 1) * $perPage)
            ->take($perPage)
            ->get();

        $formattedPosts = $posts->map(function ($post) {
            return [
                'id' => $post->id,
                'user' => $post->user->name,
                'category' => $post->category->name,
                'title' => $post->title,
                'content' => $post->content,
                'created_at' => $post->created_at,
                'updated_at' => $post->updated_at
            ];
        });

        return [
            'data' => $formattedPosts,
            'total' => $totalPosts,
            'perPage' => $perPage,
            'currentPage' => $page,
        ];
    }

    public function select($postId)
    {
        $post = Post::with(['category', 'user'])->find($postId);

        if (!$post) {
            return response()->json(['retorno' => 'erro', 'mensagem' => 'Post não existe']);
        }

        $formattedPost = [
            'id' => $post->id,
            'user_id' => $post->user->id,
            'user' => $post->user->name,
            'category' => $post->category->name,
            'category_id' => $post->category->id,
            'title' => $post->title,
            'content' => $post->content,
            'created_at' => $post->created_at,
            'updated_at' => $post->updated_at
        ];

        return $formattedPost;
    }

    public function add(Request $request)
    {
        try {
            $user = auth()->user();
            $post = $user->posts()->create($request->all());

            $post->load('category');

            $formattedPost = [
                'id' => $post->id,
                'user' => $post->user->name,
                'category' => $post->category->name,
                'title' => $post->title,
                'content' => $post->content,
                'created_at' => $post->created_at,
                'updated_at' => $post->updated_at
            ];

            return response()->json(['retorno' => 'Post criado!', 'post' => $formattedPost]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function update(Request $request, $postId)
    {
        try {
            $post = new Post();
            $postSelected =$post->with(['user', 'category'])->find($postId);

            if (!$postSelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Post não existe']);
            }

            $user = auth()->user();
            if ($postSelected->user_id !== $user->id) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Você não tem permissão para editar este post']);
            }

            $postSelected->update($request->all());

            $formattedPost = [
                'id' => $postSelected->id,
                'user' => $postSelected->user->name,
                'category' => $postSelected->category->name,
                'title' => $postSelected->title,
                'content' => $postSelected->content,
                'created_at' => $postSelected->created_at,
                'updated_at' => $postSelected->updated_at
            ];

            return response()->json(['retorno' => 'Post atualizado!', 'updated_data' => $formattedPost]);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }

    public function delete($postId)
    {
        try {
            $post = new Post();
            $postSelected = $post->find($postId);

            if (!$postSelected) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Post não existe']);
            }

            $user = auth()->user();
            if ($postSelected->user_id !== $user->id) {
                return response()->json(['retorno' => 'erro', 'mensagem' => 'Você não tem permissão para deletar este post']);
            }

            $postSelected->delete();

            return response()->json(['retorno' => 'Post deletado']);
        } catch (\Exception $error) {
            return response()->json(['retorno' => 'erro', 'details' => $error->getMessage()]);
        }
    }
}
