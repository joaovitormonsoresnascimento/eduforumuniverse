<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run(): void
    {
        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Usuario de Teste',
            'email' => 'test@example.com',
        ]);

        \App\Models\Category::factory()->create([
            'name' => 'Dúvidas',
        ]);

        \App\Models\Category::factory()->create([
            'name' => 'Comunicados',
        ]);

        \App\Models\Post::factory(10)->create();

        \App\Models\Comment::factory(10)->create();
    }
}
